package Test;

import org.testng.annotations.Test;

public class SampleTestNG4 {

	@Test
	public void WebLoginHomeDemo()
	{
		System.out.println("login to Home app");
	}
	@Test
	public void MobileLoginHomeDemo()
	{
		System.out.println("Home Mobile Login");
	}
	@Test
	public void APILoginHomeDemo()
	{
		System.out.println("Home API LogIn");
	}
}
